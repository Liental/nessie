#include <algorithm>
#include "keyboard.h"

using namespace keyboard;

// Namespace containing for useful tools for working around emulation 
namespace utilities {
  // Number of buttons connected to the device
  const unsigned int BUTTONS_LENGTH = 8;

  // Number of milliseconds used to sleep
  const unsigned int SLEEP_TIME = 5;

  // Enum containing flags that emulated controller uses
  enum ControllerKeys {
    A_KEY = 8, B_KEY = 9,
    UP_KEY = 3, DOWN_KEY = 5,
    LEFT_KEY = 2, RIGHT_KEY = 4,
    START_KEY = 6, SELECT_KEY = 7
  };
  
  /* Find given value in given array
    
    @param a - array used to find the value
    @param value - value to look for
  */
  int find_value(int a[BUTTONS_LENGTH], int value) {
    for (int i = 0; i < BUTTONS_LENGTH; i++) {
      if (a[i] == value)
        return i;
    }
    
    return -1;
  }

  /* Clone values of given array into another array

    @param a - array to clone
    @param b - array to put values in
  */
  void clone_array(int a[BUTTONS_LENGTH], int b[BUTTONS_LENGTH]) {
    for (int i = 0; i < BUTTONS_LENGTH; i++)
      b[i] = a[i];
  }

  /* Compare given arrays, return true if equal and false if not

    @param a - array to be compared with
    @param b - array to compare with array a
  */
  bool compare_arrays (int a[BUTTONS_LENGTH], int b[BUTTONS_LENGTH]) {
    sort(a, a + BUTTONS_LENGTH);
    sort(b, b + BUTTONS_LENGTH);

    for (int i = 0; i < BUTTONS_LENGTH; i++) {
      if (a[i] != b[i])
        return false;
    }

    return true;
  }

  /* Print values of given array

    @param a - array to print
  */
  void print_array (int a[BUTTONS_LENGTH]) {
    cout << "[ ";

    for (int i = 0; i < BUTTONS_LENGTH; i++)
      cout << a[i] << (i == BUTTONS_LENGTH - 1 ? " " : ", ");

    cout << "]" << endl;
  }
  /* Get keyboard key based on provided controller flag

    @param key - a controller key flag
  */
  int get_keyboard_key (int key) {
    int keyboard_key = 0;
    
    switch (key) {
      case ControllerKeys::LEFT_KEY:
        keyboard_key = KeyboardKeys::LEFT_KEY;
        break;
      case ControllerKeys::RIGHT_KEY:
        keyboard_key = KeyboardKeys::RIGHT_KEY;
        break;
      case ControllerKeys::UP_KEY:
        keyboard_key = KeyboardKeys::UP_KEY;
        break;
      case ControllerKeys::DOWN_KEY:
        keyboard_key = KeyboardKeys::DOWN_KEY;
        break;
      case ControllerKeys::A_KEY: 
        keyboard_key = KeyboardKeys::DOT_KEY;
        break;
      case ControllerKeys::B_KEY:
        keyboard_key = KeyboardKeys::COMMA_KEY;
        break;
      case ControllerKeys::START_KEY:
        keyboard_key = KeyboardKeys::ENTER_KEY;
        break;
      case ControllerKeys::SELECT_KEY:
        keyboard_key = KeyboardKeys::SHIFT_KEY;
        break;
    }

    return keyboard_key;
  }

  /* Handle controller input based on provided data

    @param key - a controller key that you want to emulate
    @param keyup - optional bool used to determine if the key has to be released or pressed (default false)  
  */
  int handle_input (int key, bool keyup = false) {
    int success;
    int kb_key = get_keyboard_key(key);

    if (key >= 100 || key == 0 || kb_key == 0)
      return 1;

    if (!keyup) 
      success = simulate_key(kb_key);
    else 
      success = simulate_keyup(kb_key);

    cout << "+ " << kb_key << (keyup ? " [KEYUP]" : " [KEYPRESS]") << endl;
    
    return success; 
  }

  /* Handle controller input based on provided data

    @param keys - array containing controller keys to emulate
    @param keyup - optional bool used to determine if the key has to be released or pressed (default false)  
  */
  void handle_input (int keys[BUTTONS_LENGTH], bool keyup = false) {
    for (int i = 0; i < BUTTONS_LENGTH; i++) {
      int success = handle_input(keys[i], keyup);

      while (success == 0)
        success = handle_input(keys[i], keyup);
    }
  }

  /* Release last_keys array keys that are not included into keys array

    @param keys - array with the recent keys
    @param last_keys - array with the keys from previous iternation that contains keys to compare  
  */
  void release_keys (int keys[BUTTONS_LENGTH], int last_keys[BUTTONS_LENGTH]) {
    int index = 0;
    int not_found[BUTTONS_LENGTH] = { 0 };

    for (int i = 0; i < BUTTONS_LENGTH; i++) {
      if (find_value(keys, last_keys[i]) == -1)
        not_found[index++] = last_keys[i];
    }
    
    for (int i = 0; i < BUTTONS_LENGTH; i++) {
      if (not_found[i] < 100)
        handle_input(not_found[i], true);
    }
  }
}