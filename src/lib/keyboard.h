#include <iostream>
#include <windows.h>

using namespace std;

// Class containing static methods used to simulate key press and release 
namespace keyboard {
    // An enum containing flags for windows keys to press.
    enum KeyboardKeys {
      UP_KEY = 72, DOWN_KEY = 80,
      LEFT_KEY = 75, RIGHT_KEY = 77,
      SHIFT_KEY = 54, SPACE_KEY = 57,
      ALT_KEY = 56, ENTER_KEY = 28,

      W_KEY = 17, S_KEY = 31,
      A_KEY = 30, D_KEY = 32,
      Z_KEY = 44,

      DOT_KEY = 52, COMMA_KEY = 51
    };

    /* Method to simulate key press event for given keycode

      @param key - keycode to the key that you want to press.
      @param keyup - optional argument to determine if the key has to be released.
    */
    int simulate_key (int key, bool keyup = false) {
      INPUT input = { 0 };

      input.ki.wScan = key;
      input.type = INPUT_KEYBOARD;
      input.ki.dwFlags = KEYEVENTF_SCANCODE;

      if (keyup) 
        input.ki.dwFlags = KEYEVENTF_SCANCODE | KEYEVENTF_KEYUP;

      int success = SendInput(1, &input, sizeof(input));

      return success;
    }

    /* Method to simulate key up event for given keycode.

      @param key - keycode to the key that you want to release.
    */
    int simulate_keyup (int key) {
      return simulate_key(key, true);
    }
};