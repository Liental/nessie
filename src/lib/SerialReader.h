#include <iostream>
#include <string>

using namespace std;

// Class used to communicate with serial devices over port 9600
class SerialReader {
  private:
    DWORD error;
    HANDLE handle;
    COMSTAT status;
    
  public:
    /* SerialReader constructor opnning device handle and setting up serial configuration 

      @param filename - path to the serial port
    */
    SerialReader (string filename) {
      HANDLE device = CreateFileA(
        (LPCSTR) filename.c_str(),
        GENERIC_READ, 0, NULL, 
        OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL
      );

      if (device != INVALID_HANDLE_VALUE)
        this->handle = device;
      else {
        cout << GetLastError() << endl;
        CloseHandle(this->handle);

        return;
      }

      DCB serialParams;
      GetCommState(this->handle, &serialParams);

      serialParams.ByteSize = 8;
      serialParams.Parity = NOPARITY;
      serialParams.BaudRate = CBR_9600;
      serialParams.StopBits = ONESTOPBIT;
      serialParams.fDtrControl = DTR_CONTROL_ENABLE;
  
      SetCommState(this->handle, &serialParams);
    }

    ~SerialReader () {
      CloseHandle(this->handle);
    }

    /* Communicate with the device to receive a message.
       In case of an error, return error code, otherwise, return 0.
      
       @param buffer - pointer to the buffer you want your message written into
       @param size - maximum size of incoming message (MAX_SIZE = 255)
    **/
    int read_message (void* buffer, unsigned int size) {
      DWORD bytes_read;
      unsigned int to_read = size;

      bool clear_success = ClearCommError(this->handle, &this->error, &this->status);

      if (!clear_success) {
        cout << "Error while clearing communication error: " << GetLastError() << endl;
        return GetLastError();
      }

      if (to_read > this->status.cbInQue)
        to_read = this->status.cbInQue;

      bool read_success = ReadFile(this->handle, buffer, to_read, &bytes_read, NULL);
      
      if (!read_success) {
        cout << "Error while reading incoming message: " << GetLastError() << endl;
        return GetLastError();
      }

      return 0;
    }
};