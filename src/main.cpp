#include "./lib/utilities.h"
#include "./lib/SerialReader.h"

using namespace std;
using namespace keyboard;
using namespace utilities;

int main () {
	// Is the application synced with device
	bool is_sync = false;

	// Table containing the result message
	int result[BUTTONS_LENGTH] = { 0 };

	// Table containing last result to use when message is not received and to check what keys to release
	int last_result[BUTTONS_LENGTH] = { 0 };

	// Table to check if the result is nulled (no message was sent)
	int not_received[BUTTONS_LENGTH] = { 0 };

	// Table to check if no buttons were pressed
	int not_pressed[BUTTONS_LENGTH] = { 102, 103, 104, 105, 106, 107, 108, 109 };

	SerialReader reader("\\\\.\\COM3");
	cout << "+ Connection established" << endl;

	// Press NumLock to allow usage of arrows
	simulate_key(0x45);

	while (true) {
		// Clear the result so the comparation doesnt break
		fill_n(result, BUTTONS_LENGTH, 0);

		for (int i = 0; i < BUTTONS_LENGTH; i++) {
			int buffer = 0;
			reader.read_message(&buffer, 1);

			// Make sure that the results dont mix up
			if (find_value(result, buffer) == -1) {
				result[i] = buffer;
				continue;
			}

			// In case the results did mix up, go back and take next message
			i -= 1;
		}

		bool is_same = compare_arrays(result, last_result);
		bool is_not_pressed = compare_arrays(result, not_pressed);
		bool is_not_received = compare_arrays(result, not_received);
		bool is_last_not_pressed = compare_arrays(last_result, not_pressed);
		
		if (!is_not_received && !is_sync)
			is_sync = true;

		if (is_not_received)
			clone_array(last_result, result);

		if (!is_not_pressed) 
			handle_input(result);
			
		if (!is_same && !is_last_not_pressed)
			release_keys(result, last_result);

		clone_array(result, last_result);

		if (is_sync)
			Sleep(SLEEP_TIME);
	}

	return 0;
}