const unsigned int buttons[] = { 2, 3, 4, 5, 6, 7, 8, 9 };
const unsigned int BUTTONS_LENGTH = 8;
const unsigned int SLEEP_TIME = 5;

void setup () {
  for (int i = 0; i < BUTTONS_LENGTH; i++)
    pinMode(buttons[i], INPUT);
    
  Serial.begin(9600);
}

/* Get current states of buttons.
   If pressed set item to the button pin,
   otherwise set it to the button pin + 100.

  @param a - table to save the states in.
*/
void get_states (byte a[BUTTONS_LENGTH]) {
  for (int i = 0; i < BUTTONS_LENGTH; i++) {
    int button = buttons[i];
    
    if (digitalRead(button) == HIGH)
      a[i] = button;
    else
      a[i] = 100 + button;
  }
}

void loop () {
  byte to_send[BUTTONS_LENGTH];
  get_states(to_send);

  Serial.write(to_send, BUTTONS_LENGTH);

  delay(SLEEP_TIME);
}
