# Nessie

Nessie is a Windows NES gamepad emulator. It connects with the software through Serial 
communication and interprets the massages with predefined button IDs to simulate 
keyboard key presses. The application will simulate the keys that are assigned to the 
default controls on Nestopia to make the usage easier.

### Technology used
+ [Arduino](https://www.arduino.cc/)
+ [MinGW compiler](http://www.mingw.org/)

### Example of an arduino setup
![example](https://gitlab.com/Liental/nessie/raw/master/assets/example.png "Example of an arduino setup")